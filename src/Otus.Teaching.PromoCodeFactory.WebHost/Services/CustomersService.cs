﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Google.Protobuf;
using GrpcServices;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Castle.Core.Resource;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Collections;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomerShortResponseCollection> GetCustomers(Empty request, ServerCallContext context)
        {
            IEnumerable<Customer> customers = await _customerRepository.GetAllAsync();
           
            var query = 
                from c in customers
                select new CustomerShortResponse
                {
                    Id = new UUID { Value = c.Id.ToString() },
                    Email = c.Email,
                    FirstName = c.FirstName,
                    LastName = c.LastName
                };
        
            return new CustomerShortResponseCollection { Customers = { query.ToList() } };
        }

        public override async Task<CustomerResponse> GetCustomer(UUID request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Value));

            var preferences = customer.Preferences.Select(p =>
                new PreferenceResponse
                {
                    Id = new UUID { Value = p.Preference.Id.ToString() },
                    Name = p.Preference.Name
                });

            return new CustomerResponse
            {
                Id = new UUID { Value = customer.Id.ToString() },
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = { preferences }
            };
        }

        public override async Task<UUID> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            List<Guid> preferenceIds = request.PreferenceIds
                .Select(uuid => Guid.Parse(uuid.Value))
                .ToList();

            IEnumerable<Preference> preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);

            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            customer.Preferences = preferences.Select(x => new CustomerPreference
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.AddAsync(customer);

            return new UUID { Value = customer.Id.ToString() };
        }

        public override async Task<Empty> EditCustomers(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id.Value));

            List<Guid> preferenceIds = request.PreferenceIds
                .Select(uuid => Guid.Parse(uuid.Value))
                .ToList();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);


            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(p => new CustomerPreference
            {
                CustomerId = customer.Id,
                Preference = p,
                PreferenceId = p.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(UUID request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Value));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
